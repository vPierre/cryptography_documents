Security Policy
==============================================================================

Security Announcements
------------------------------------------------------------------------------

Join the GitLab issues for security and vulnerability announcements.

Atomist Open Source Security Policies and Procedures
------------------------------------------------------------------------------

This document outlines security procedures and general policies for the
ndaal Open Source projects as found on https://gitlab.com/ndaal_open_source/.

  * `Reporting a Vulnerability`_
  * `Disclosure Policy`_

Instructions for Reporting a Vulnerability
------------------------------------------------------------------------------

Instructions for reporting a vulnerability can be found on the
[ndaal Security and Disclosure Information] page.

Supported Versions
------------------------------------------------------------------------------

Information about supported versions can be found on the repo.

Reporting a Vulnerability
------------------------------------------------------------------------------

The ndaal team and community take all security vulnerabilities
seriously. Thank you for improving the security of our open source
software. We appreciate your efforts and responsible disclosure and will
make every effort to acknowledge your contributions.

Report security vulnerabilities by emailing digitally signed the ndaal security team at:

    `security@ndaal.eu <mailto://security@ndaal.eu>`_

The lead maintainer will acknowledge your email within 24 hours, and will
send a more detailed response within 48 hours indicating the next steps in
handling your report. After the initial reply to your report, the security
team will endeavor to keep you informed of the progress towards a fix and
full announcement, and may ask for additional information or guidance.

Report security vulnerabilities in third-party modules to the person or
team maintaining the module.

Disclosure Policy
------------------------------------------------------------------------------

When the security team receives a security bug report, they will assign it
to a primary handler. This person will coordinate the fix and release
process, involving the following steps:

  * Confirm the problem and determine the affected versions.
  * Audit code to find any potential similar problems.
  * Prepare fixes for all releases still under maintenance. These fixes
    will be released as fast as possible to NPM.


Create high secure hash values like SHA-512 and SHA3-512.

Notes
------------------------------------------------------------------------------

The content of SECURITY.md and SECURITY.rst is equal.<br>
For further information, c.f. https://github.com/ossf/scorecard/blob/main/docs/checks.md#security-policy
